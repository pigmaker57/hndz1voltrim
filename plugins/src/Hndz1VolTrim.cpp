/*
 * Performance-oriented stereo volume trim effect built with DISTRHO Plugin Framework (DPF)
 *
 * SPDX-License-Identifier: MIT
 *
 * By Pigmaker57 <kaitarou@gmail.com>
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to
 * deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
 * sell copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 * FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS
 * IN THE SOFTWARE.
 */

#include "DistrhoPlugin.hpp"

#include <vector>
#include <cmath>

START_NAMESPACE_DISTRHO

#ifndef DB_CO
#define DB_CO(g) ((g) > -90.0f ? powf(10.0f, (g) * 0.05f) : 0.0f)
#endif

#define TWO_PI 6.283185307179586476925286766559f

// -----------------------------------------------------------------------------------------------------------

// One-pole LPF for smooth parameter changes
// Originaly from: 
// 1) https://www.musicdsp.org/en/latest/Filters/257-1-pole-lpf-for-smooth-parameter-changes.html
// 2) https://github.com/osam-cologne/stereogain
class CParamSmooth {
public:
    CParamSmooth(float smoothing_ms, float sample_rate) {
        configure(smoothing_ms, sample_rate);
    }

    ~CParamSmooth() { }

    void configure(float smoothing_ms, float sample_rate) {
        t = smoothing_ms;

        if (sample_rate != csr) {
            csr = sample_rate;
            a = exp(-TWO_PI / (t * 0.001f * sample_rate));
            b = 1.0f - a;
            z = 0.0f;
        }
    }

    inline float process(float in) {
        return z = (in * b) + (z * a);
    }
private:
    float a = 0.0f, b = 0.0f, t = 0.0f, z = 0.0f;
    float csr = 0.0f;
};

// --------------------------------------
// Main plugin class
class Hndz1VolTrim : public Plugin
{
private:

    enum {
        PARAM_VOL = 0,
        PARAM_MIN_VOL,
        PARAM_MAX_VOL,
        PARAM_EXP,
        PARAM_SMOOTHING_TIME,
        
        //
        PARAMS_COUNT
    };

    struct ParamInfo {
        String name;
        float min_value;
        float def_value;
        float max_value;
        uint32_t hints;
        String unit;

        ParamInfo(const char* name_, float min_value_, float def_value_, float max_value_, uint32_t hints_, const char* unit_ = nullptr)
            : name(name_), min_value(min_value_), def_value(def_value_), max_value(max_value_), hints(hints_), unit((unit_? unit_ : ""))
        {}
    };

    ParamInfo param_info[PARAMS_COUNT] = {
        ParamInfo("Vol adjust", 0.0f, 100.0f, 100.0f, kParameterIsAutomable, "%"),
        ParamInfo("Min vol", -90.0f, -70.0f, 0.0f, kParameterIsAutomable, "dB"),
        ParamInfo("Max vol", -90.0f, 0.0f, 0.0f, kParameterIsAutomable, "dB"),
        ParamInfo("Vol law exp", 1.0f, 0.5f,  7.0f, kParameterIsAutomable),
        ParamInfo("Smooting time", 1.0f, 20.0f, 100.0f, kParameterIsAutomable, "ms"),
    }; 

    float param_values[PARAMS_COUNT] = {};

    float curr_sample_rate = 0;

    float target_gain_factor = -1.0f;  

    CParamSmooth *smooth_vol;

    float gainFactor() {
        float vol_min = param_values[PARAM_MIN_VOL];
        float vol_max = param_values[PARAM_MAX_VOL]; 
        float vol_factor = param_values[PARAM_VOL] / 100.0f;
        float vol_law_exp = param_values[PARAM_EXP];
        
        if(vol_max - vol_min < 0) return 0.0f;

        if(vol_law_exp > 1.01f) vol_factor = std::pow(vol_factor, vol_law_exp);

        float vol_db = vol_min + vol_factor*(vol_max - vol_min);

        return DB_CO(vol_db);
    }

public:
    Hndz1VolTrim()
        : Plugin(PARAMS_COUNT, 0, 0) // PARAMS_COUNT parameters, 0 programs, 0 states
    {
        for(size_t i=0; i < PARAMS_COUNT; i++) {
            param_values[i] = param_info[i].def_value;
        }

        target_gain_factor = gainFactor();
        curr_sample_rate = getSampleRate();

        smooth_vol = new CParamSmooth(param_values[PARAM_SMOOTHING_TIME], curr_sample_rate);
    }

    ~Hndz1VolTrim() {
        delete smooth_vol;
    }

protected:

    void sampleRateChanged(double new_sample_rate) {
        curr_sample_rate = new_sample_rate;
        smooth_vol->configure(param_values[PARAM_SMOOTHING_TIME], new_sample_rate);
    }

   /* --------------------------------------------------------------------------------------------------------
    * Process */

   /**
      Run/process function for plugins without MIDI input.
    */
    void run(const float** inputs, float** outputs, uint32_t frames) override {
        if(target_gain_factor < 0.0f) target_gain_factor = gainFactor();

        const float* in1  =  inputs[0];
        const float* in2  =  inputs[1];
        float* out1 = outputs[0];
        float* out2 = outputs[1];

        for(uint32_t nf = 0; nf < frames; nf++) {
            float gain_factor = smooth_vol->process(target_gain_factor);

            (*out1++) = (*in1++) * gain_factor;
            (*out2++) = (*in2++) * gain_factor;
        }
    }

    // -------------------------------------------------------------------------------------------------------

   /* --------------------------------------------------------------------------------------------------------
    * Information */

   /**
      Get the plugin label.
      A plugin label follows the same rules as Parameter::symbol, with the exception that it can start with numbers.
    */
    const char* getLabel() const override {
        return "Hndz1VolTrim";
    }

   /**
      Get an extensive comment/description about the plugin.
    */
    const char* getDescription() const override {
        return "Performance-oriented stereo volume trim";
    }

   /**
      Get the plugin author/maker.
    */
    const char* getMaker() const override
    {
        return "pigmaker57";
    }

   /**
      Get the plugin homepage.
    */
    const char* getHomePage() const override
    {
        return "https://";
    }

   /**
      Get the plugin license name (a single line of text).
      For commercial plugins this should return some short copyright information.
    */
    const char* getLicense() const override
    {
        return "ISC";
    }

   /**
      Get the plugin version, in hexadecimal.
    */
    uint32_t getVersion() const override
    {
        return d_version(1, 0, 0);
    }

   /**
      Get the plugin unique Id.
      This value is used by LADSPA, DSSI and VST plugin formats.
    */
    int64_t getUniqueId() const override
    {
        return d_cconst('H', '1', 'V', 'T');
    }

   /* --------------------------------------------------------------------------------------------------------
    * Init */

   /**
      Initialize the parameter @a index.
      This function will be called once, shortly after the plugin is created.
    */
    void initParameter(uint32_t index, Parameter& parameter) override
    {
       /**
          Changing parameters does not cause any realtime-unsafe operations, so we can mark them as automable.
          Also set as boolean because they work as on/off switches.
        */
        parameter.hints = param_info[index].hints;
        parameter.ranges.min = param_info[index].min_value;
        parameter.ranges.max = param_info[index].max_value;
        parameter.ranges.def = param_info[index].def_value;;
        parameter.name = param_info[index].name;
        parameter.unit = param_info[index].unit;

       /**
          Our parameter names are valid symbols except for "-".
        */
        parameter.symbol = parameter.name;
        parameter.symbol.replace('-', '_');
    }

   /* --------------------------------------------------------------------------------------------------------
    * Internal data */

   /**
      Get the current value of a parameter.
    */
    float getParameterValue(uint32_t index) const override {
        return param_values[index];
    }

   /**
      Change a parameter value.
    */
    void setParameterValue(uint32_t index, float value) override {
        param_values[index] = value;

        switch(index) {
            case PARAM_SMOOTHING_TIME:
                smooth_vol->configure(param_values[PARAM_SMOOTHING_TIME], curr_sample_rate);
                break;

            default:
                target_gain_factor = gainFactor();
                break;
        }
    }

private:

   /**
      Set our plugin class as non-copyable and add a leak detector just in case.
    */
    DISTRHO_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR(Hndz1VolTrim)
};

/* ------------------------------------------------------------------------------------------------------------
 * Plugin entry point, called by DPF to create a new plugin instance. */

Plugin* createPlugin()
{
    return new Hndz1VolTrim();
}

// -----------------------------------------------------------------------------------------------------------

END_NAMESPACE_DISTRHO
