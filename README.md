# Hndz1VolTrim

Performance-oriented stereo volume trim effect built with DISTRHO Plugin Framework (DPF)

## Compiling

    $ cd plugins/src
    $ make

Binaries will be put in bin/ directory at project root
